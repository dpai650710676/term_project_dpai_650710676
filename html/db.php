<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>DB</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Krub:wght@200&display=swap" rel="stylesheet">
  <style type="text/css">
    body {
      font-family: 'Krub', sans-serif;
    }
  </style>
</head>
<body style="background-color: #e3f2fd;">
  <nav class="navbar bg-dark" data-bs-theme="dark">
    <div class="container-fluid">
      <a class="navbar-brand" href="#"><font color="#F9FFFF"><b> Term_Project </b></font></a>
      <button class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Select</a>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="index.html">Profile</a></li>
              <li><a class="dropdown-item" href="interested.html">Interested</a></li>
              <li><a class="dropdown-item" href="about_su.html">About Silpakorn</a></li>
              <li><a class="dropdown-item" href="db.php">Database</a></li>
            </ul>
          </li>
        </div>
      </div>
    </div>
  </nav>
  <section>
    <center>
      <h2><b><p style="font-size:30px"></p><font color="#041777"> Database </font></b></h2>
    </center>
  </section>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>

  <?php
    $servername = "db";
    $username = "devops";
    $password = "devops101";

    $dbhandle = mysqli_connect($servername, $username, $password);
    $selected = mysqli_select_db($dbhandle, "titanic");
    
    echo "Connected database server<br>";
    echo "Selected database";
  ?>
</body>
</html>
